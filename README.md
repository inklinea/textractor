Textractor:

First Draft - Appears in the extension menu under AAA.

Inkscape extension ***  REQUIRES INKSCAPE 1.1+ ****

Basic text extraction from svg files.

Does not handle overlapping text correctly - yet :)

If settings have been saved and Inkscape exited cleanly, it can also be used from the command line.

e.g. 

inkscape --batch-process --actions="org.inkscape.textractor.noprefs;" ./text_test.svg 

The above will extract text using Inkscape from the command line.

( Provided there is a path set in the extension dialogue in Inkscape and save is checked in that dialogue. )



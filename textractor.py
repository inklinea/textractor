#!/usr/bin/env python
# coding=utf-8
#
# Copyright (C) [2021] [Matt Cottam], [mpcottam@raincloud.co.uk]
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
#
#
# #############################################################################
# Textractor - Inkscape Extension - basic text extraction from svg files.
# #############################################################################

import inkex
import operator
import random
import os
from pathlib import Path


def list_to_text(self, my_text_list, depth):
    final_text = ''
    if depth > 1:
        my_text_string_list = [x[0] for x in my_text_list]
    else:
        my_text_string_list = my_text_list
    for item in my_text_string_list:
        final_text += ' '.join(str(x) for x in item)
        final_text += '\n'


    if self.options.preview_text == 'true':
        inkex.errormsg(final_text)

    if self.options.save_text == 'true':
        try:
            random_number = random.randrange(10000000, 99999999)
            save_path = self.options.save_path
            filename = self.svg.name

            if filename.endswith('.svg'):
                filename = filename[:-4]

            export_filename_path = str(save_path) + str(os.sep) + str(filename) + str(random_number) + '.txt'

            f = open(export_filename_path, "w")
            f.write(final_text)
            f.close()
        except:
            None

    return final_text


def get_attributes(self):
    for att in dir(self):
        inkex.errormsg((att, getattr(self, att)))

class Textractor(inkex.EffectExtension):

    def add_arguments(self, pars):
        pars.add_argument("--text_tag_order", type=str, dest="text_tag_order", default='text_svg_code_order')
        pars.add_argument("--preview_text", type=str, dest="preview_text", default='selected')
        pars.add_argument("--save_text", type=str, dest="save_text", default='selected')

        pars.add_argument("--save_path", type=str, dest="save_path", default=str(Path.home()))


    def effect(self):

        if self.options.text_tag_order == 'text_svg_code_order':

            document_text_elements = self.svg.xpath('//svg:text')

            if len(document_text_elements) > 0:

                my_text_list = []

                for my_text_element in document_text_elements:
                    my_text = my_text_element.xpath('.//text()')
                    my_text_list.append(my_text)

            final_text = list_to_text(self, my_text_list, 1)



        if self.options.text_tag_order == 'text_top_left_bottom_right_order':

            document_text_elements = self.svg.xpath('//svg:text')

            if len(document_text_elements) > 0:

                my_text_list = []

                for my_text_element in document_text_elements:
                    my_text_list_item = []
                    my_text = my_text_element.xpath('.//text()')
                    text_left = my_text_element.bounding_box().left
                    text_top = my_text_element.bounding_box().top

                    my_text_list_item.append(my_text)
                    my_text_list_item.append(text_left)
                    my_text_list_item.append(text_top)
                    my_text_list.append(my_text_list_item)
                    # inkex.errormsg(f'Left {text_left} Top {text_top}')

                my_text_list = sorted(my_text_list, key=operator.itemgetter(2, 1))

                final_text = list_to_text(self, my_text_list, 2)


        if self.options.text_tag_order == 'text_selection_order':

            document_text_elements = self.svg.selected

            if len(document_text_elements) > 0:

                my_text_list = []

                for my_text_element in document_text_elements:

                    if my_text_element.TAG == 'text':
                        my_text = my_text_element.xpath('.//text()')
                        my_text_list.append(my_text)

                final_text = list_to_text(self, my_text_list, 1)




if __name__ == '__main__':
    Textractor().run()
